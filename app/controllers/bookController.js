'use strict';

var books = require('google-books-search');

var Favorite = require('../model/favoriteModel.js');

var options = {
    field: 'title',
    offset: 0,
    limit: 10,
    type: 'books',
    order: 'relevance',
    lang: 'en'
};


exports.search_books = function(req, res) {

  options.lang = req.query.lang;

  books.search(req.query.q, options, function(error, results) {
      if ( ! error ) {
        res.send(results);
      } else {
          console.log(error);
      }
  });

};


exports.update_favorite = function(req, res) {

  Favorite.getFavoriteById(req.query.id_book, function(err, favorite) {
    if (err){
      res.send(err);
    }else{
      if(favorite.length == 0){

        var new_favorite = new Favorite(req.query);

        Favorite.updateFavorite(new_favorite, function(err, favorite) {

          if (err){
            res.send(err);
          }else{
            res.send({status: 'created'});
            //res.json(new_favorite);
          }

        });

      }else {

        Favorite.remove( req.query.id_book, function(err, task) {
          if (err){
            res.send(err);
          }else{
            res.send({status: 'deleted'});
          }
        });

      }
    }
  });

}

exports.list_all_favorites = function(req, res) {

  Favorite.getAllFavorites(function(err, favorite) {

    console.log('controller')
    if (err){
      res.send(err);
    }else{
      console.log('res', favorite);
      res.send(favorite);
    }

  });

};
