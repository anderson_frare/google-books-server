'use strict';

module.exports = function(app) {
var bookList = require('../controllers/bookController');

// todoList Routes
app.route('/tasks')

   app.route('/books')
   .get(bookList.search_books)
   .put(bookList.update_favorite);

   app.route('/books/favorites')
   .get(bookList.list_all_favorites);

};
