'user strict';

var connection = require('../../db.js');

var sql = connection;

//Favorite object constructor
var Favorite = function(book){
    this.id_book = book.id_book;
    this.created_at = new Date();
};
Favorite.updateFavorite = function (newFavorite, result) {
        sql.query("INSERT INTO favorites set ?", newFavorite, function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });
};

Favorite.getAllFavorites = function (result) {
    sql.query("Select * from favorites", function (err, res) {

            if(err) {
                console.log("error: ", err);
                result(null, err);
            }
            else{
              console.log('favorites : ', res);
             result(null, res);
            }
        });
};

Favorite.getFavoriteById = function (favoriteId, result) {

    sql.query("Select id from favorites where id_book = ? ", favoriteId, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);

        }
    });

};

Favorite.remove = function(id, result){
    sql.query("DELETE FROM favorites WHERE id_book = ?", [id], function (err, res) {

               if(err) {
                   console.log("error: ", err);
                   result(null, err);
               }
               else{

                result(null, res);
               }
           });
};

module.exports= Favorite;