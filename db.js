'user strict';

var mysql = require('mysql');

//local mysql db connection
var connection = mysql.createConnection({
    port: 8889,
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'google_books'
});

connection.connect(function(err) {
    if (err) throw err;
});

module.exports = connection;